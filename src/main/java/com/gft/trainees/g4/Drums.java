package com.gft.trainees.g4;

import javax.sound.midi.*;

public class Drums implements IInstrumentoPercusion {

    private boolean afinado;
    private Patch drums;
    private final MidiChannel channel;
    private double[] parches;

    public Drums(Synthesizer midiSynth, int channelNumber) {
        parches = new double[2];
        for (int i = 0; i < parches.length; i++) {
            parches[i] = Math.random() * 2 - 1;
        }
        channel = midiSynth.getChannels()[channelNumber];
        final Instrument[] instr = midiSynth.getDefaultSoundbank().getInstruments();
        for (int i = 0; i < instr.length; i++) {
            if (instr[i].getName().contains("Tom")) {
                System.out.println(i + ": " + instr[i].getName());
                midiSynth.loadInstrument(instr[i]);
                drums = instr[i].getPatch();
                channel.programChange(drums.getBank(), drums.getProgram());
                break;
            }
        }
        System.out.println("Afinacion Tambores: " + getAfinacion());
    }

    @Override
    public synchronized void tocar(final Nota nota) {
        System.out.println("Drums: " + nota+ " A:" + getAfinacion());
        channel.noteOn( afinado ? nota.n() : Math.random()>0.5 ? nota.n()+1: nota.n()-1, 200);
        parches[(int) (Math.random() * parches.length -1)] += Math.random() - 0.25;
        try {
            Thread.sleep(nota.t());
        } catch (final InterruptedException e) {
            System.err.println("Error: " + e.getMessage());
        }
        channel.allNotesOff();
        if ( getAfinacion() > 0.02 || getAfinacion() < -0.02) {
            afinado = false;
        }
    }

    @Override
    public boolean afinar() {
        for(int i = 0; i < parches.length; i++) {
            if ( parches[i] > 0) {
                aflojarParche(i);
            } else if (parches[i] < 0) {
                apretarParche(i);
            }
        }
        afinado = true;
        System.out.println("Los tambores han sido afinados: "+ getAfinacion());
        return afinado;
    }

    @Override
    public double aflojarParche(int parche) {
        parches[parche] = 0;
        return 0;
    }

    @Override
    public double apretarParche(int parche) {
        parches[parche] = 0;
        return 0;
    }

    @Override
    public boolean cambiarParche(int parche) {
        if ( parche < 0 || parche > parches.length -1) {
            return false;
        }
        parches[parche] = 0;
        return true;
    }

    private double getAfinacion() {
        double afinacion = 0;
        for ( double p : parches) {
            afinacion += p;
        }
        afinacion /= parches.length;
        return afinacion;
    }
}