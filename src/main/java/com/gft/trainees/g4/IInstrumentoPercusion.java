package com.gft.trainees.g4;

public interface IInstrumentoPercusion extends IInstrumento {
    double aflojarParche(int parche);
    double apretarParche(int parche);
    boolean cambiarParche(int parche);
}