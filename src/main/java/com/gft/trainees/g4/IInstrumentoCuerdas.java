package com.gft.trainees.g4;

public interface IInstrumentoCuerdas extends IInstrumento {
    double aflojarCuerda(int cuerda, double giros);
    double apretarCuerda(int cuerda, double giros);
    boolean cambiarCuerda(int cuerda, int tipoCuerda);
}