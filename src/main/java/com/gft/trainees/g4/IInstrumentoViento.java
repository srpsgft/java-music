package com.gft.trainees.g4;

public interface IInstrumentoViento extends IInstrumento{
    boolean limpiar();
    boolean desarmar();
    boolean cambiarBoquilla(int boquilla);
}