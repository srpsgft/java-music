package com.gft.trainees.g4;

import java.util.ArrayList;
import java.util.List;

public class Song {
    List<Nota> notas;

    public Nota[] getArray() {
        return notas.toArray(new Nota[notas.size()]);
    }

    public Song addNota(String nota, int tempo) {
        this.notas.add(new Nota(nota, tempo));
        return this;
    }

    public Song addNota(String nota) {
        this.notas.add(new Nota(nota, 1000));
        return this;
    }

    public Song() {
        this.notas = new ArrayList<Nota>();
    }

    public void play(IInstrumento instrument) {
        for (Nota n : notas) {
            instrument.tocar(n);
        }
    }
}