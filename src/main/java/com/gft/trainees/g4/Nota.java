package com.gft.trainees.g4;

public class Nota {
    private int notaN;
    private int tempo;
    private String notaS;
    public int n() {
        return notaN;
    }
    public int t() {
        return tempo;
    }
    @Override
    public String toString() {
        return notaS;
    }
    public Nota (final String nota, final int tempo) {
        this.notaS = nota;
        this.tempo = tempo;
        char[] sonido = nota.toCharArray();
        int notaInt = 0; 
        int index = 0;
        switch (sonido[index++]) {
            case 'C':
                notaInt = 12;
                break;
            case 'D':
                notaInt = 14;
                break;
            case 'E':
                notaInt = 16;
                break;
            case 'F':
                notaInt = 17;
                break;
            case 'G':
                notaInt = 19;
                break;
            case 'A':
                notaInt = 21;
                break;
            case 'B':
                notaInt = 23;
                break;
            default:
                notaInt = 0;
                break;
        }
        if(sonido.length > 2) {
            if(sonido[index++] == '#') {
                notaInt++;
            } else {
                notaInt--;
            }
        }
        notaInt += Integer.parseInt(String.valueOf(sonido[index])) * 12;
        this.notaN = notaInt;
    }
}