package com.gft.trainees.g4;

public interface IInstrumento {
    void tocar (final Nota nota);
    boolean afinar ();
}