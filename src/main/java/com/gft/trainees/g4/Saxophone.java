package com.gft.trainees.g4;
import javax.sound.midi.*;
public class Saxophone implements IInstrumentoViento {

    private boolean afinado;
    private Patch saxophone;
    private final MidiChannel channel;
    private double boquilla;
    private double dirtyRate;

    public Saxophone (Synthesizer midiSynth, int channelNumber) {
        dirtyRate = Math.random()*0.5;
        boquilla = Math.random();
        channel = midiSynth.getChannels()[channelNumber];
        final Instrument[] instr = midiSynth.getDefaultSoundbank().getInstruments();
        for (int i = 0; i < instr.length; i++) {
            if (instr[i].getName().contains("Tenor Sax")) {
                System.out.println(i + ": " + instr[i].getName());
                midiSynth.loadInstrument(instr[i]);
                saxophone = instr[i].getPatch();
                channel.programChange(saxophone.getBank(),saxophone.getProgram());
                break;
            }
        }
        System.out.println("Afinacion Saxophone: " + getAfinacion());
    }

    @Override
    public synchronized void tocar(final Nota nota) {
        System.out.println("Saxophone: " + nota + " A: " + getAfinacion() );
        channel.noteOn( afinado ? nota.n() : Math.random()>0.5 ? nota.n()+1: nota.n()-1, 200);
        boquilla += Math.random() * dirtyRate; 
        dirtyRate += 0.001;
        try {
            Thread.sleep(nota.t());
        } catch (final InterruptedException e) {
            System.err.println("Error: "+e.getMessage());
        }
        channel.allNotesOff();
        if ( getAfinacion() > 0.1 || getAfinacion() < -0.1) {
            afinado = false;
        }
    }

    @Override
    public boolean afinar() {
        desarmar();
        limpiar();
        afinado = true;
        System.out.println("El saxophon ha sido afinado: "+ getAfinacion());
        return afinado;
    }

    @Override
    public boolean limpiar() {
        dirtyRate = 0.01;
        return true;
    }

    @Override
    public boolean desarmar() {
        boquilla = 0;
        return true;
    }

    @Override
    public boolean cambiarBoquilla(int boquilla) {
        boquilla = 0;
        dirtyRate = 0;
        return afinado;
    }

    private double getAfinacion() {
        return boquilla;
    }

}