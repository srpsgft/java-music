package com.gft.trainees.g4;

import javax.sound.midi.*;

public class Piano implements IInstrumentoCuerdas {

    private boolean afinado;
    private final double cuerdas[];
    private Patch piano;
    private final MidiChannel channel;

    public Piano(final Synthesizer midiSynth, final int channelNumber) {
        cuerdas = new double[224];
        for (int i = 0; i < cuerdas.length; i++) {
            cuerdas[i] = Math.random() * 2 - 1;
        }
        channel = midiSynth.getChannels()[channelNumber];
        final Instrument[] instr = midiSynth.getDefaultSoundbank().getInstruments();
        for (int i = 0; i < instr.length; i++) {
            if (instr[i].getName().contains("Piano")) {
                System.out.println(i + ": " + instr[i].getName());
                midiSynth.loadInstrument(instr[i]);
                piano = instr[i].getPatch();
                channel.programChange(piano.getBank(), piano.getProgram());
                break;
            }
        }
        System.out.println("Afinacion Piano: " + getAfinacion());
    }

    @Override
    public synchronized void tocar(final Nota nota) {
        System.out.println("Piano: " + nota + " A: "+ getAfinacion());
        channel.noteOn( afinado ? nota.n() : Math.random()>0.5 ? nota.n()+1: nota.n()-1 , 200);
        cuerdas[(int) (Math.random() * cuerdas.length -1)] += Math.random() - 0.25;
        try {
            Thread.sleep(nota.t());
        } catch (final InterruptedException e) {
            System.err.println("Error: " + e.getMessage());
        }
        channel.allNotesOff();
        if ( getAfinacion() > 0.02 || getAfinacion() < -0.02) {
            afinado = false;
        }
    }

    @Override
    public boolean afinar() {
        for (int i = 0; i < cuerdas.length; i++) {
            if (cuerdas[i] > 0) {
                aflojarCuerda(i, cuerdas[i]);
            } else if (cuerdas[i] < 0) {
                apretarCuerda(i, cuerdas[i]);
            }
        }
        afinado = true;
        System.out.println("El Piano ha sido afinado: " + getAfinacion());
        return afinado;
    }

    @Override
    public double aflojarCuerda(final int cuerda, final double giros) {
        cuerdas[cuerda] -= giros;
        return cuerdas[cuerda];
    }

    @Override
    public double apretarCuerda(final int cuerda, final double giros) {
        cuerdas[cuerda] -= giros;
        return cuerdas[cuerda];
    }

    @Override
    public boolean cambiarCuerda(final int cuerda, final int tipoCuerda) {
        cuerdas[cuerda] = 0;
        return true;
    }

    private double getAfinacion() {
        double afinacion = 0;
        for (double c : cuerdas) {
            afinacion += c;
        }
        afinacion /= cuerdas.length;
        return afinacion;
    }
}